package com.supermarket;
import java.util.Scanner;

class Player{
    public static void main(String[] args) {
        Supermarket sp = new Supermarket();
        int halt;
        do{
            halt = sp.goToRemoval();
        } while(halt != 4);
    }
}

public class Supermarket {
    Scanner sc = new Scanner(System.in);
    public int goToRemoval(){
        System.out.println("1. Products Removal");
        System.out.println("2. SweetShop");
        System.out.println("3. Clothes");
        System.out.println("4. Quit");
        int choise = sc.nextInt();
        if(choise == 1) {
            Supermarket playerLocation = new FoodRemoval();
            playerLocation.getProducts();
        }else if(choise == 2) {
            Supermarket playerLocation = new SweetShop();
            playerLocation.getProducts();
        }else if(choise == 3) {
            Supermarket playerLocation = new Clothes();
            playerLocation.getProducts();
        }
        return choise;
    }
    public void getProducts(){

    }
}

class FoodRemoval extends Supermarket{
    String products[] = {"Fish", "Meat", "Milk"};

    @Override
    public void getProducts() {
        System.out.println("1. Fish");
        System.out.println("2. Meat");
        System.out.println("3. Milk");
        System.out.println("4. Quit");
        int choise = sc.nextInt();
        if(choise == 1){
            System.out.println("You bought fish");
        }else if(choise == 2){
            System.out.println("You bought meat");
        }else if(choise == 3){
            System.out.println("You bought milk");
        }
        return;
    }
}

class SweetShop extends Supermarket{
    String products[] = {"Candies", "Cake", "Chocolate"};

    @Override
    public void getProducts() {
        System.out.println("1. Candies");
        System.out.println("2. Cake");
        System.out.println("3. Milk");
        System.out.println("4. Chocolate");
        int choise = sc.nextInt();
        if(choise == 1){
            System.out.println("You bought Candies");
        }else if(choise == 2){
            System.out.println("You bought Cake");
        }else if(choise == 3){
            System.out.println("You bought Chocolate");
        }
        return;
    }
}

class Clothes extends Supermarket{
    String products[] = {"Pants", "T-Shirts", "Fleep-Flops"};

    @Override
    public void getProducts() {
        System.out.println("1. Pants");
        System.out.println("2. T-Shirts");
        System.out.println("3. Fleep-Flops");
        System.out.println("4. Quit");
        int choise = sc.nextInt();
        if(choise == 1){
            System.out.println("You bought Pants");
        }else if(choise == 2){
            System.out.println("You bought T-Shirts");
        }else if(choise == 3){
            System.out.println("You bought Fleep-Flops");
        }
        return;
    }
}